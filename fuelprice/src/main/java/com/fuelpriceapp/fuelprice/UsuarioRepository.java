package com.fuelpriceapp.fuelprice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class UsuarioRepository {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public int cadastrarUsuario(Usuario usuario) {
		
		if(usuario.getLoginadmin() == "" || usuario.getSenhaadmin() == "") {
			try {
				return jdbcTemplate.update("INSERT INTO USUARIO (ID, NOME, LOGIN, SENHA, ADMIN) " + "VALUES(?, ?,  ?, ?, ?)",
						new Object[] { usuario.getId(), usuario.getNome(), usuario.getLogin(), usuario.getSenha(), usuario.getAdmin() });
			}catch(Exception e) {
				return 12;
			}
		}
		else {
			if(checaAdmin(usuario.getLoginadmin(), usuario.getSenhaadmin()) != null) {
				try {
					return jdbcTemplate.update("INSERT INTO USUARIO (ID, NOME, LOGIN, SENHA, ADMIN) " + "VALUES(?, ?,  ?, ?, ?)",
							new Object[] { usuario.getId(), usuario.getNome(), usuario.getLogin(), usuario.getSenha(), usuario.getAdmin() });
				}catch(Exception e) {
					return 12;
				}
			}
			else {
				return 0;
			}
		}
		
	}
	
	public Usuario checaAdmin(String login, String senha) {
		try {
			return jdbcTemplate.queryForObject("SELECT LOGIN, SENHA FROM USUARIO WHERE ADMIN LIKE 'S' AND LOGIN LIKE ? AND SENHA LIKE ?", 
					new Object[] { login, senha },
					new BeanPropertyRowMapper<Usuario>(Usuario.class));
		}catch(Exception e) {
			System.out.println("Login ou senha de administrador incorreta.");
			return null;
		}
	}
	
	public Usuario checaUsuarioSimples(String login, String senha) {
		try {
			return jdbcTemplate.queryForObject("SELECT LOGIN, SENHA FROM USUARIO WHERE ADMIN LIKE 'N' AND LOGIN LIKE ? AND SENHA LIKE ?", 
					new Object[] { login, senha },
					new BeanPropertyRowMapper<Usuario>(Usuario.class));
		}catch(Exception e) {
			System.out.println("Login ou senha incorreta.");
			return null;
		}
	}
	
	public List<Usuario> listaUsuario(String login, String senha) {
		if(checaAdmin(login, senha) != null) {
				return jdbcTemplate.query("SELECT ID, NOME, LOGIN, SENHA ADMIN FROM USUARIO", new Object[] {},
						new BeanPropertyRowMapper<Usuario>(Usuario.class));
		}
		else {
			//select apenas dos postos
			if(checaUsuarioSimples(login, senha) != null) {
					return jdbcTemplate.query("SELECT NOME FROM USUARIO", new Object[] {},
							new BeanPropertyRowMapper<Usuario>(Usuario.class));
			}
			else {
				return null;
			}
		}
	}
	
	public int deletarUsuario(Usuario usuario) {
		try {
			return jdbcTemplate.update("DELETE FROM USUARIO WHERE LOGIN LIKE ?",
			new Object[] { usuario.getLogin() });
		}catch(Exception e) {
			return 12;
		}
	 }
	
	public int atualizarUsuario(Usuario usuario) {
		try {
			return jdbcTemplate.update("UPDATE USUARIO SET NOME = ?, LOGIN = ?, SENHA = ? WHERE LOGIN LIKE ?",
			new Object[] { usuario.getNome(), usuario.getLogin(), usuario.getSenha(), usuario.getLogin()});
		}catch(Exception e) {
			return 12;
		}
	 }

}
