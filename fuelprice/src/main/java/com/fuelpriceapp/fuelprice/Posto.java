package com.fuelpriceapp.fuelprice;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity(name="Posto")
@Data
public class Posto {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@Column(name="regiao", length=2)
	private String regiao;
	
	@Column(name="estado", length=2)
	private String estado;
	
	@Column(name="municipio", length=100)
	private String municipio;
	
	@Column(name="revenda", nullable=false, unique=true, length=150)
	private String revenda;
	
	@Column(name="produto", length=50)
	private String produto;
	
	@Column(name="data_coleta", length=10)
	private String dataColeta;
	
	@Column(name="valor_compra", length=10)
	private String valorCompra;
	
	@Column(name="valor_venda", length=10)
	private String valorVenda;
	
	@Column(name="unidade_medida", length=20)
	private String unidadeMedida;
	
	@Column(name="bandeira", length=100)
	private String bandeira;

}
