package com.fuelpriceapp.fuelprice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {
	private UsuarioRepository usuRep;
	//private PostoRepository postoRep;
	private List<Usuario> usuList;
	
	@Autowired
    public HomeController(UsuarioRepository usuRep) {
          this.usuRep = usuRep;
    }
	
	@RequestMapping("/")
    public String home(@ModelAttribute Usuario usuario) {
        return "index";
    }
	
	@PostMapping("/")
	public String login(@ModelAttribute Usuario usuario, @RequestParam("login") String login, 
			@RequestParam("senha") String senha, Model model) {
		usuList = usuRep.listaUsuario(login, senha);
		if(usuList == null) {
			return "index";
		}
		else {
			if(usuList.toString().contains(login)) {
	            model.addAttribute("usuarios", usuList);
	            //a lista completa irá listar usuários e postos com a opção de CRUD para ambos
	            //só os adms podem ver e editar a lista completa
	            
	            int result = 0;    //postoRep.importCsv(); --> o método não está pronto
	    		if(result == 0) {
	    			model.addAttribute("erroimport", "Erro ao importar arquivo csv.");
	    			return "listacompleta";
	    		}
	    		else {
	    			return "listacompleta"; //passar os parâmetros para listar os postos quando conseguir importar o arquivo...
	    		}
			}
			else {
				//a lista simples irá listar apenas os postos com a opção de CRUD
				return "listasimples";
			}
		}
	}
	
	@RequestMapping("/listacompleta")
	public String deletaUsuario(@ModelAttribute Usuario usuario, @RequestParam("nome") String nome, @RequestParam("login") String login, 
			@RequestParam("senha") String senha) {
		usuario.setNome(nome);
		usuario.setLogin(login);
		usuario.setSenha(senha);
		usuRep.deletarUsuario(usuario);
		HomeController hc = new HomeController(usuRep);
		Model model = null;
		return hc.login(usuario, login, senha, model);
	}
	
	@RequestMapping("/cadastro")
	public String cadastrar(@ModelAttribute Usuario usuario) {
		return "cadastro";
	}
	
	@PostMapping("/cadastro")
	public String cadastro(@ModelAttribute Usuario usuario, @RequestParam("nome") String nome, @RequestParam("login") String login, 
			@RequestParam("senha") String senha, @RequestParam("admin") String admin, @RequestParam("loginadmin") String loginadmin, 
			@RequestParam("senhaadmin") String senhaadmin) {
		usuario.setId(usuario.hashCode());
		usuario.setNome(nome);
		usuario.setLogin(login);
		usuario.setSenha(senha);
		usuario.setAdmin(admin);
		usuario.setLoginadmin(loginadmin);
		usuario.setSenhaadmin(senhaadmin);
		int result = usuRep.cadastrarUsuario(usuario);
		if(result == 0) {
			return "naocadastrado";
		}
		else {
			if(result == 12) {
				return "cadastro";
			}
			else {
				return "cadastrado";
			}
		}
	}
	
}
