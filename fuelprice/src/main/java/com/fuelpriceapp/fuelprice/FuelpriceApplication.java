package com.fuelpriceapp.fuelprice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FuelpriceApplication {

	public static void main(String[] args) {
		SpringApplication.run(FuelpriceApplication.class, args);
	}

}

