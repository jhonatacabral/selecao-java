package com.fuelpriceapp.fuelprice;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import lombok.Data;

@Entity(name = "Usuario")
@Data
public class Usuario {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@Column(name="nome", nullable=false, length=100)
	private String nome;
	
	@Column(name="login", nullable=false, unique=true, length=20)
    private String login; 

	@Column(name="senha", nullable=false, length=20)
	private String senha; 
	
	@Column(name="admin", nullable=false, length=1)
	private String admin;
	
	@Transient
	private String loginadmin;
	@Transient
	private String senhaadmin;
   
}
