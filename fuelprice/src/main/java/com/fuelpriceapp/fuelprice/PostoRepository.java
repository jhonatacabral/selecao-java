package com.fuelpriceapp.fuelprice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class PostoRepository {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public int importCsv() {
		
		try {
			jdbcTemplate.update("INSERT INTO POSTO (ID, REGIAO, ESTADO, MUNICIPIO, REVENDA, PRODUTO, DATA_COLETA, VALOR_COMPRA, VALOR_VENDA, "
					+ "UNIDADE_MEDIDA, BANDEIRA)     SELECT \\\"ID\\\", \\\"REGIAO\\\", "
					+ "\\\"ESTADO\\\", \\\"MUNICIPIO\\\", \\\"REVENDA\\\", \\\"PRODUTO\\\", \\\"DATA_COLETA\\\", "
					+ "\\\"VALOR_COMPRA\\\", \\\"VALOR_VENDA\\\", \\\"UNIDADE_MEDIDA\\\", \\\"BANDEIRA\\\" "
					+ "FROM CSVREAD( 'C:\\\\tmp\\\\POSTOS.csv', 'REGIAO, ESTADO, MUNICIPIO, REVENDA, PRODUTO, DATA_COLETA, VALOR_COMPRA, "
					+ "VALOR_VENDA, UNIDADE_MEDIDA, BANDEIRA' ) ",
					new Object[] {});
			return 1;
		}catch(Exception e) {
			return 0;
		}
	}

}
